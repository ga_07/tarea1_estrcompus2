TARGET = tarea1

CCX = g++
CFLAGS = -Wall -g

LINKER = g++
LFLAGS = -I.

SRCDIR = src
OBJDIR = build
BINDIR = bin
INCDIR = include
DOCDIR = doc

SOURCES := $(wildcard $(SRCDIR)/*.cpp)
INCLUDES := $(wildcard $(INCDIR)/*.h)
OBJECTS := $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
rm = rm -rf


all: $(BINDIR)/$(TARGET)


$(BINDIR)/$(TARGET): $(OBJECTS)
	@$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
	@echo "Linker done"


$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@$(CCX) $(CFLAGS) -I$(INCDIR) -c $< -o $@
	@echo "Compiling done"

run: 
	gunzip -c branch-trace-gcc.trace.gz | head -5000 | $(BINDIR)/$(TARGET) -s 4 -bp 3 -gh 3 -ph 3 -o 1

doc:
	@doxygen Doxyfile
	@echo "Documentation done"


clean:
	@$(rm) $(DOCDIR)/*
	@$(rm) $(OBJECTS)
	@$(rm) $(OBJDIR)/*.o
	@$(rm) $(BINDIR)/$(TARGET)


.PHONY: all clean doc