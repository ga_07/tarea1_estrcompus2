#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <fstream>

using namespace std;

//Clase con los métodos para simular los distintos predictores de saltos.
class Predictor{
    //Atributos
    private:
        int s;   //Tamaño de la tabla BTH
        int bp;  //Tipo de predicción
        int gh;  //Tamaño del registro de predicción global
        int ph;  //Tamaño de los registros de historia privada
        int out; //Salida de la simulación
        vector<int> address;    //Vector para guardar las máscara de s bits de las direcciones de memoria. 
        vector<string> jumps;   //Vector para guardar la información de los saltos que están en el archivo comprimido.
        vector<string> decimal; //Vector para guardar las direcciones de memoria tal cual se leen; se asumió una codificación en decimal.
    //Métodos
    public:
        //Constructor de la clase
        Predictor(int s,int bp,int gh,int ph,int out);
        //Destructor de la clase
        ~Predictor();
        //Método para obtener los datos del archivo comprimido
        void read();
        //Método para simular un predictor bimodal.
        void bimodal();
        //Método para simular un predictor PShare.
        void pshare();
        //Método para simular un predictor GShare.
        void gshare();
        //Método para simular un predictor por torneo.
        void tournament();
        //Método para la salida de la simulación.
        void writeFile(vector<string> decimal,vector<string> jumps,vector<string> prediction, vector<string> check,string name,int BHT);
};