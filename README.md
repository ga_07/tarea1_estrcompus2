# Tarea1 Estructuras de Computadores Digitales II

* Gabriel Gutiérrez Arguedas B63215
________________________________________________
________________________________________________

# Instrucciones de uso:
A continuación se muestran los comandos que debe ejecutar según la operación que desee realizar. Se
incluye un archivo Makefile; para cambir los parámetros de la simulación proceda a editar la línea
34 del archivo mencionado

#### Compilar el programa
    $ make
    
#### Ejecutar el programa
    $ make run

#### Ejecutar Doxygen para la documentación
    $ make doc

#### Limpieza de directorios
    $ make clean