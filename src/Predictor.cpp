#include "Predictor.h"

//Constructor de la clase. Los parámetros son recibidos por argumento desde la función main.
Predictor::Predictor(int s,int bp,int gh,int ph,int out){
    this->s = s;     //Tamaño de la tabla BTH
    this->bp = bp;   //Tipo de predicción
    this->gh = gh;   //Tamaño del registro de predicción global
    this->ph = ph;   //Tamaño de los registros de historia privada
    this->out = out; //Salida de la simulación
}

//Destructor de la clase. No se le implementa una funcionalidad.
Predictor::~Predictor(){
    //Does nothing
}

//Método para obtener los datos del archivo comprimido.
void Predictor::read(){
    string str;                               //Declaración del string para leer los datos
    while(!cin.eof()){                        //Mientras la entrada del programa sea distinto de endo of file
        cin >> str;                           //Redirige la entrada del programa al string
        if((str != "T") && (str != "N")){     //Revisa si la entrada es distinta de un taken o si es distinta de not taken
            decimal.push_back(str);           //Guarda la dirección de memoria codificada en decimal en su respectivo vector
            long int num = stol(str,nullptr); //Método de la clase string para convertirlo en un long integer
            int id = pow(2,this->s) - 1;      //Calcula la cantidad de bits de la máscara de la dirección de memoria
            int bin = num & id;               //Obtiene los s bits de la máscara de la dirección de memoria, ya que la AND fuerza ceros
            address.push_back(bin);           //Guarda la máscara de s bits de la dirección de memoria en su respectivo vector
        }
        else {
            jumps.push_back(str);             //Si no se da el case anterior sólo se guarda la información del salto en su respectivo vector
        }    
    }
}

//Método para simular un predictor bimodal
void Predictor::bimodal(){
    int counters = pow(2,this->s);              //Calcula el tamaño de la BHT
    string predic;                              //Declara un string para realizar la predicción
    string states[counters];                    //Declara un arreglo del tamaño de la BHT para almacenar los estados
    for(int j = 0; j < counters; j++){ 
        states[j] = "SN";                       //Inicia todos los contadores en Stongly Not Taken
    }
    vector<string> prediction;                  //Declaración de un vector para almacenar las predicciones
    vector<string> check;                       //Declaración de un vector para almacenar el resultado de las predicciones    
    int id = address[0];                        //El índice para los contadores de la BHT es la máscara de s bits de la dirección de memoria
    for(size_t i = 0; i < address.size(); i++){     
        if(states[id] == "SN"){                 //Revisa si el estado del contador es Strongly Not Taken
            predic = "N";                       //Si cumple lo anterior predice un Not Taken
            prediction.push_back(predic);       //Guarda la predicción en su respectivo vector
            if(predic == jumps[i]){             //Revisa si la predicción es igual al salto
                check.push_back("correct");     //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector    
                id = address[i+1];              //Actualiza el índice de los contadores de la BHT a la siguiente máscara de s bits
            }
            else{
                check.push_back("incorrect");   //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WN";              //Actualiza el estado a Weakly Not Taken
                id = address[i+1];              //Actualiza el índice de los contadores de la BHT a la siguiente máscara de s bits
            }
        }
        else if(states[id] == "WN"){            //Revisa si el estado del contador es Weakly Not Taken
            predic = "N";                       //Si cumple lo anterior predice un Not Taken
            prediction.push_back(predic);       //Guarda la predicción en su respectivo vector
            if(predic == jumps[i]){             //Revisa si la predicción es igual al salto
                check.push_back("correct");     //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
                states[id] = "SN";              //Actualiza el estado a Strongly Not Taken
                id = address[i+1];              //Actualiza el índice de los contadores de la BHT a la siguiente máscara de s bits
            }
            else{
                check.push_back("incorrect");   //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WT";              //Actualiza el estado a Weakly Taken
                id = address[i+1];              //Actualiza el índice de los contadores de la BHT a la siguiente máscara de s bits
            }
        }
        else if(states[id] == "WT"){            //Revisa si el estado del contador es Weakly Taken
            predic = "T";                       //Si cumple lo anterior predice un Taken
            prediction.push_back(predic);       //Guarda la predicción en su respectivo vector
            if(predic == jumps[i]){             //Revisa si la predicción es igual al salto
                check.push_back("correct");     //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
                states[id] = "ST";              //Actualiza el estado a Strongly Taken
                id = address[i+1];              //Actualiza el índice de los contadores de la BHT a la siguiente máscara de s bits
            }
            else{
                check.push_back("incorrect");   //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WN";              //Actualiza el estado a Weakly Not Taken
                id = address[i+1];              //Actualiza el índice de los contadores de la BHT a la siguiente máscara de s bits
            }
        }
        else if(states[id] == "ST"){            //Revisa si el estado del contador es Strongly Taken
            predic = "T";                       //Revisa si el estado del contador es Weakly Take
            prediction.push_back(predic);       //Guarda la predicción en su respectivo vector
            if(predic == jumps[i]){             //Revisa si la predicción es igual al salto
                check.push_back("correct");     //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
                id = address[i+1];              //Actualiza el índice de los contadores de la BHT a la siguiente máscara de s bits
            }
            else{
                check.push_back("incorrect");   //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WT";              //Actualiza el estado a Weakly Taken
                id = address[i+1];              //Actualiza el índice de los contadores de la BHT a la siguiente máscara de s bits
            }
        }
    }
    if(this->out == 1){     //Si la salida de la simulación es 1 llama a la función writeFile
        writeFile(decimal,jumps,prediction,check,"bimodal.txt",counters);
    }
}

//Método para simular un predictor PShare
void Predictor::pshare(){
    int counters = pow(2,this->s);                  //Calcula el tamaño de la BHT
    string predic;                                  //Declara un string para realizar la predicción
    int id;                                         //Declara el índice para los contadores de la BHT
    int sbits;                                      //Declara una variable para la máscara de s bits
    int ph_bits = pow(2,this->ph) - 1;              //Calcula los bits de los registros de historia privada
    int regPHT = 0 & ph_bits;                       //Inicia un registro de la PHT en cero de tamaño ph bits
    string states[counters];                        //Declara un arreglo del tamaño de la BHT para almacenar los estados
    int history[counters];                          //Declara un arreglo del tamaño de la BHT para almacenar la historia privada
    for(int j = 0; j < counters; j++){
        states[j] = "SN";                           //Inicia todos los contadores en Stongly Not Taken
        history[j] = regPHT;                        //Inicia todos los registros privados en cero
    }
    vector<string> prediction;                      //Declaración de un vector para almacenar las predicciones
    vector<string> check;                           //Declaración de un vector para almacenar el resultado de las predicciones 
    for(size_t i = 0; i < address.size(); i++){
        sbits = address[i];                         //Obtiene la máscara de s bits de la dirección de memoria 
        regPHT = history[sbits];                    //Obtiene el registro de la PHT
        id = sbits ^ regPHT;                        //Calcula el índice de la BHT como la XOR del registro de la PHT con la máscara de s bits de la dirección de memoria
        if(states[id] == "SN"){                     //Revisa si el estado del contador es Strongly Not Taken
            predic = "N";                           //Si cumple lo anterior predice un Not Taken
            prediction.push_back(predic);           //Guarda la predicción en su respectivo vector
            if(prediction[i] == jumps[i]){          //Revisa si la predicción es igual al salto
                check.push_back("correct");         //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
            }
            else{
                check.push_back("incorrect");       //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WN";                  //Actualiza el estado a Weakly Not Taken
            }
            if(jumps[i] == "T"){                    //Revisa si el salto fue un Taken
                regPHT = (regPHT << 1) + 1;         //Aplica shift left al registro y le suma 1
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
            else {
                regPHT = (regPHT << 1);             //Hubo un Not Taken, sólo aplica shift left al registro
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
        }
        else if(states[id] == "WN"){                //Revisa si el estado del contador es Weakly Not Taken
            predic = "N";                           //Si cumple lo anterior predice un Not Taken
            prediction.push_back(predic);           //Guarda la predicción en su respectivo vector
            if(prediction[i] == jumps[i]){          //Revisa si la predicción es igual al salto
                check.push_back("correct");         //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
                states[id] = "SN";                  //Actualiza el estado a Strongly Not Taken
            }
            else{
                check.push_back("incorrect");       //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WT";                  //Actualiza el estado a Weakly Taken
            }
            if(jumps[i] == "T"){                    //Revisa si el salto fue un Taken
                regPHT = (regPHT << 1) + 1;         //Aplica shift left al registro y le suma 1
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
            else {
                regPHT = (regPHT << 1);             //Hubo un Not Taken, sólo aplica shift left al registro
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
        }
        else if(states[id] == "WT"){                //Revisa si el estado del contador es Weakly Taken
            predic = "T";                           //Si cumple lo anterior predice un Taken
            prediction.push_back(predic);           //Guarda la predicción en su respectivo vector
            if(prediction[i] == jumps[i]){          //Revisa si la predicción es igual al salto
                check.push_back("correct");         //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
                states[id] = "ST";                  //Actualiza el estado a Strongly Taken
            }
            else{
                check.push_back("incorrect");       //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WN";                  //Actualiza el estado a Weakly Not Taken
            }
            if(jumps[i] == "T"){                    //Revisa si el salto fue un Taken
                regPHT = (regPHT << 1) + 1;         //Aplica shift left al registro y le suma 1
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
            else {
                regPHT = (regPHT << 1);             //Hubo un Not Taken, sólo aplica shift left al registro
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
        }
        else if(states[id] == "ST"){                //Revisa si el estado del contador es Strongly Taken
            predic = "T";                           //Si cumple lo anterior predice un Taken
            prediction.push_back(predic);           //Guarda la predicción en su respectivo vector
            if(prediction[i] == jumps[i]){          //Revisa si la predicción es igual al salto
                check.push_back("correct");         //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
            }
            else{
                check.push_back("incorrect");       //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WT";                  //Actualiza el estado a Weakly Taken
            }
            if(jumps[i] == "T"){                    //Revisa si el salto fue un Taken
                regPHT = (regPHT << 1) + 1;         //Aplica shift left al registro y le suma 1
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
            else {
                regPHT = (regPHT << 1);             //Hubo un Not Taken, sólo aplica shift left al registro
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            } 
        }
    }
    if(this->out == 1){     //Si la salida de la simulación es 1 llama a la función writeFile
        writeFile(decimal,jumps,prediction,check,"pshare.txt",counters);
    }
}

//Método para simular un predictor GShare
void Predictor::gshare(){
    int counters = pow(2,this->s);              //Calcula el tamaño de la BHT
    string predic;                              //Declara un string para realizar la predicción
    string states[counters];                    //Declara un arreglo del tamaño de la BHT para almacenar los estados
    for(int j = 0; j < counters; j++){
        states[j] = "SN";                       //Inicia todos los contadores en Stongly Not Taken
    }
    vector<string> prediction;                  //Declaración de un vector para almacenar las predicciones
    vector<string> check;                       //Declaración de un vector para almacenar el resultado de las predicciones
    int reg = 0;                                //Declaración e inicialización del registro global en cero
    int gh_bits = pow(2,this->gh) - 1;          //Calcula los bits del registro de historia global
    int hist = reg & gh_bits;                   //Inicia la historia del registro global en cero de tamaño gh bits
    int id_g = hist ^ address[0];               //Calcula el índice de la BHT como la XOR del registro global con la máscara de s bits de la dirección de memoria
    int id = id_g & (counters - 1);             //Fuerza el índice de la BHT a tener la cantidad de bits que espacios de la BHT
    for(size_t i = 0; i < address.size(); i++){
        if(states[id] == "SN"){                 //Revisa si el estado del contador es Strongly Not Taken
            predic = "N";                       //Si cumple lo anterior predice un Not Taken
            prediction.push_back(predic);       //Guarda la predicción en su respectivo vector
            if(prediction[i] == jumps[i]){      //Revisa si la predicción es igual al salto
                check.push_back("correct");     //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
            }
            else{
                check.push_back("incorrect");   //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WN";              //Actualiza el estado a Weakly Not Taken
            }
            if(jumps[i] == "T"){                //Revisa si el salto fue un Taken
                reg = (reg << 1) + 1;           //Aplica shift left al registro y le suma 1
                hist = reg & gh_bits;           //Fuerza a la historia con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id = id_g & (counters - 1);     //Fuerza el índice de la BHT a tener la cantidad de bits que espacios de la BHT
            }
            else {
                reg = (reg << 1);               //Hubo un Not Taken, sólo aplica shift left al registro
                hist = reg & gh_bits;           //Fuerza a la historia con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id = id_g & (counters - 1);     //Fuerza el índice de la BHT a tener la cantidad de bits que espacios de la BHT
            }
        }
        else if(states[id] == "WN"){            //Revisa si el estado del contador es Weakly Not Taken
            predic = "N";                       //Si cumple lo anterior predice un Not Taken
            prediction.push_back(predic);       //Guarda la predicción en su respectivo vector
            if(prediction[i] == jumps[i]){      //Revisa si la predicción es igual al salto
                check.push_back("correct");     //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
                states[id] = "SN";              //Actualiza el estado a Strongly Not Taken
            }
            else{
                check.push_back("incorrect");   //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WT";              //Actualiza el estado Weakly Taken
            }
            if(jumps[i] == "T"){                //Revisa si el salto fue un Taken
                reg = (reg << 1) + 1;           //Aplica shift left al registro y le suma 1
                hist = reg & gh_bits;           //Fuerza a la historia con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id = id_g & (counters - 1);     //Fuerza el índice de la BHT a tener la cantidad de bits que espacios de la BHT
            }
            else {
                reg = (reg << 1);               //Hubo un Not Taken, sólo aplica shift left al registro
                hist = reg & gh_bits;           //Fuerza a la historia con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id = id_g & (counters - 1);     //Fuerza el índice de la BHT a tener la cantidad de bits que espacios de la BHT
            }
        }
        else if(states[id] == "WT"){            //Revisa si el estado del contador es Weakly Taken
            predic = "T";                       //Si cumple lo anterior predice un Taken
            prediction.push_back(predic);       //Guarda la predicción en su respectivo vector
            if(prediction[i] == jumps[i]){      //Revisa si la predicción es igual al salto
                check.push_back("correct");     //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
                states[id] = "ST";              //Actualiza el estado Strongly Taken
            }
            else{
                check.push_back("incorrect");   //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WN";              //Actualiza el estado Weakly Not Taken
            }
            if(jumps[i] == "T"){                //Revisa si el salto fue un Taken
                reg = (reg << 1) + 1;           //Aplica shift left al registro y le suma 1
                hist = reg & gh_bits;           //Fuerza a la historia con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id = id_g & (counters - 1);     //Fuerza el índice de la BHT a tener la cantidad de bits que espacios de la BHT
            }
            else {
                reg = (reg << 1);               //Hubo un Not Taken, sólo aplica shift left al registro
                hist = reg & gh_bits;           //Fuerza a la historia con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id = id_g & (counters - 1);     //Fuerza el índice de la BHT a tener la cantidad de bits que espacios de la BHT
            }
        }
        else if(states[id] == "ST"){            //Revisa si el estado del contador es Weakly Taken
            predic = "T";                       //Si cumple lo anterior predice un Taken
            prediction.push_back(predic);       //Guarda la predicción en su respectivo vector
            if(prediction[i] == jumps[i]){      //Revisa si la predicción es igual al salto
                check.push_back("correct");     //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
            }
            else{
                check.push_back("incorrect");   //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
                states[id] = "WT";              //Actualiza el estado Weakly Taken
            }
            if(jumps[i] == "T"){                //Revisa si el salto fue un Taken 
                reg = (reg << 1) + 1;           //Aplica shift left al registro y le suma 1
                hist = reg & gh_bits;           //Fuerza a la historia con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id = id_g & (counters - 1);     //Fuerza el índice de la BHT a tener la cantidad de bits que espacios de la BHT
            }
            else {
                reg = (reg << 1);               //Hubo un Not Taken, sólo aplica shift left al registro
                hist = reg & gh_bits;           //Fuerza a la historia con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id = id_g & (counters - 1);     //Fuerza el índice de la BHT a tener la cantidad de bits que espacios de la BHT
            }
        }
    }
    if(this->out == 1){     //Si la salida de la simulación es 1 llama a la función writeFile
        writeFile(decimal,jumps,prediction,check,"gshare.txt",counters);
    }
}

//Método para simular un predictor por torneo
void Predictor::tournament(){
    int counters = pow(2,this->s);      //Calcula el tamaño de la BHT
    int id_P;                           //Declara el índice para los contadores de la BHT del PShare
    int sbits;                          //Declara una variable para la máscara de s bits
    int ph_bits = pow(2,this->ph) - 1;  //Calcula los bits de los registros de historia privada
    int regPHT = 0 & ph_bits;           //Inicia un registro de la PHT en cero de tamaño ph bits
    int history[counters];              //Declara un arreglo del tamaño de la BHT para almacenar la historia privada
    int reg = 0;                        //Declaración e inicialización del registro global en cero
    int gh_bits = pow(2,this->gh) - 1;  //Calcula los bits del registro de historia global
    int hist = reg & gh_bits;           //Inicia la historia del registro global en cero de tamaño gh bits
    int id_g = hist ^ address[0];       //Calcula el índice de la BHT del GShare como la XOR del registro global con la máscara de s bits de la dirección de memoria
    int id_G = id_g & (counters - 1);   //Fuerza el índice de la BHT del GShare a tener la cantidad de bits que espacios de la BHT
    int id_meta;                        //Declara el índice para los contadores del meta predictor
    string predic;                      //Declara un string para realizar la predicción
    string states_P[counters];          //Declara un arreglo del tamaño de la BHT para almacenar los estados del PShare
    string states_G[counters];          //Declara un arreglo del tamaño de la BHT para almacenar los estados del GShare
    int states_meta[counters];          //Declara un arreglo del tamaño de la BHT para almacenar los estados del meta predictor
    vector<string> prediction_P;        //Declaración de un vector para almacenar las predicciones del PShare
    vector<string> prediction_G;        //Declaración de un vector para almacenar las predicciones del GShare
    vector<string> final_pred;          //Declaración de un vector para almacenar las predicciones del meta predictor
    vector<string> final_check;         //Declaración de un vector para almacenar el resultado de las predicciones
    for(int j = 0; j < counters; j++){
        states_P[j] = "SN";             //Inicia todos los contadores del PShare en Stongly Not Taken
        history[j] = regPHT;            //Inicia todos los registros privados en cero
        states_G[j] = "SN";             //Inicia todos los contadores del GShare en Stongly Not Taken
        states_meta[j] = 3;             //Inicia todos los contadores del meta predictor en Stongly Prefer PShare
    }
    for(size_t i = 0; i < address.size(); i++){
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //PShare
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        sbits = address[i];                         //Obtiene la máscara de s bits de la dirección de memoria
        regPHT = history[sbits];                    //Obtiene el registro de la PHT
        id_P = sbits ^ regPHT;                      //Calcula el índice de la BHT del PShare como la XOR del registro de la PHT con la máscara de s bits de la dirección de memoria
        if(states_P[id_P] == "SN"){                 //Revisa si el estado del contador es Strongly Not Taken                          
            predic = "N";                           //Si cumple lo anterior predice un Not Taken
            prediction_P.push_back(predic);         //Guarda la predicción en su respectivo vector
            if(prediction_P[i] != jumps[i]){        //Revisa si la predicción es distinta al salto
                states_P[id_P] = "WN";              //Actualiza el estado Weakly Not Taken
            }
            if(jumps[i] == "T"){                    //Revisa si el salto fue un Taken
                regPHT = (regPHT << 1) + 1;         //Aplica shift left al registro privado y le suma 1
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
            else {
                regPHT = (regPHT << 1);             //Hubo un Not Taken, sólo aplica shift left al registro privado
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
        }
        else if(states_P[id_P] == "WN"){            //Revisa si el estado del contador es Weakly Not Taken  
            predic = "N";                           //Si cumple lo anterior predice un Not Taken
            prediction_P.push_back(predic);         //Guarda la predicción en su respectivo vector
            if(prediction_P[i] == jumps[i]){        //Revisa si la predicción es igual al salto                            
                states_P[id_P] = "SN";              //Actualiza el estado Strongly Not Taken
            }
            else{
                states_P[id_P] = "WT";              //Actualiza el estado Weakly Taken
            }
            if(jumps[i] == "T"){                    //Revisa si el salto fue un Taken
                regPHT = (regPHT << 1) + 1;         //Aplica shift left al registro privado y le suma 1
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
            else {
                regPHT = (regPHT << 1);             //Hubo un Not Taken, sólo aplica shift left al registro privado
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
        }
        else if(states_P[id_P] == "WT"){            //Revisa si el estado del contador es Weakly Taken 
            predic = "T";                           //Si cumple lo anterior predice un Taken
            prediction_P.push_back(predic);         //Guarda la predicción en su respectivo vector
            if(prediction_P[i] == jumps[i]){        //Revisa si la predicción es igual al salto                
                states_P[id_P] = "ST";              //Actualiza el estado Strongly Taken
            }
            else{
                states_P[id_P] = "WN";              //Actualiza el estado Weakly Not Taken
            }
            if(jumps[i] == "T"){                    //Revisa si el salto fue un Taken
                regPHT = (regPHT << 1) + 1;         //Aplica shift left al registro privado y le suma 1
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
            else {
                regPHT = (regPHT << 1);             //Hubo un Not Taken, sólo aplica shift left al registro privado
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
        }
        else if(states_P[id_P] == "ST"){            //Revisa si el estado del contador es Strongly Taken                                
            predic = "T";                           //Si cumple lo anterior predice un Taken
            prediction_P.push_back(predic);         //Guarda la predicción en su respectivo vector
            if(prediction_P[i] != jumps[i]){        //Revisa si la predicción es distinto al salto
                states_P[id_P] = "WT";              //Actualiza el estado Weakly Taken
            }
            if(jumps[i] == "T"){                    //Revisa si el salto fue un Taken
                regPHT = (regPHT << 1) + 1;         //Aplica shift left al registro privado y le suma 1
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
            else {
                regPHT = (regPHT << 1);             //Hubo un Not Taken, sólo aplica shift left al registro privado
                history[sbits] = regPHT & ph_bits;  //Actualiza el registro de la PHT; la AND lo fuerza al tamaño de ph bits
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //GShare
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        if(states_G[id_G] == "SN"){             //Revisa si el estado del contador es Strongly Not Taken                                              
            predic = "N";                       //Si cumple lo anterior predice un Not Taken
            prediction_G.push_back(predic);     //Guarda la predicción en su respectivo vector
            if(prediction_G[i] != jumps[i]){    //Revisa si la predicción es distinto al salto
                states_G[id_G] = "WN";          //Actualiza el estado Weakly Not Taken
            }
            if(jumps[i] == "T"){                //Revisa si el salto fue un Taken
                reg = (reg << 1) + 1;           //Aplica shift left al registro global y le suma 1
                hist = reg & gh_bits;           //Fuerza a la historia global con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT del GSharecomo la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id_G = id_g & (counters - 1);   //Fuerza el índice de la BHT del GShare a tener la cantidad de bits que espacios de la BHT                                
            }
            else {
                reg = (reg << 1);               //Hubo un Not Taken, sólo aplica shift left al registro global
                hist = reg & gh_bits;           //Fuerza a la historia global con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT del GShare como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id_G = id_g & (counters - 1);   //Fuerza el índice de la BHT del GShare a tener la cantidad de bits que espacios de la BHT
            }
        }
        else if(states_G[id_G] == "WN"){        //Revisa si el estado del contador es Weakly Not Taken                                 
            predic = "N";                       //Si cumple lo anterior predice un Not Taken
            prediction_G.push_back(predic);     //Guarda la predicción en su respectivo vector
            if(prediction_G[i] == jumps[i]){    //Revisa si la predicción es igual al salto
                states_G[id_G] = "SN";          //Actualiza el estado Strongly Not Taken
            }
            else{
                states_G[id_G] = "WT";          //Actualiza el estado Weakly Taken                                  
            }
            if(jumps[i] == "T"){                //Revisa si el salto fue un Taken
                reg = (reg << 1) + 1;           //Aplica shift left al registro global y le suma 1
                hist = reg & gh_bits;           //Fuerza a la historia global con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT del GSharecomo la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id_G = id_g & (counters - 1);   //Fuerza el índice de la BHT del GShare a tener la cantidad de bits que espacios de la BHT                                
            }
            else {
                reg = (reg << 1);               //Hubo un Not Taken, sólo aplica shift left al registro global
                hist = reg & gh_bits;           //Fuerza a la historia global con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT del GShare como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id_G = id_g & (counters - 1);   //Fuerza el índice de la BHT del GShare a tener la cantidad de bits que espacios de la BHT
            }
        }
        else if(states_G[id_G] == "WT"){        //Revisa si el estado del contador es Weakly Taken 
            predic = "T";                       //Si cumple lo anterior predice un Taken
            prediction_G.push_back(predic);     //Guarda la predicción en su respectivo vector                              
            if(prediction_G[i] == jumps[i]){    //Revisa si la predicción es igual al salto
                states_G[id_G] = "ST";          //Actualiza el estado Strongly Taken
            }
            else{
                states_G[id_G] = "WN";          //Actualiza el estado Weakly Not Taken
            }
            if(jumps[i] == "T"){                //Revisa si el salto fue un Taken
                reg = (reg << 1) + 1;           //Aplica shift left al registro global y le suma 1
                hist = reg & gh_bits;           //Fuerza a la historia global con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT del GSharecomo la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id_G = id_g & (counters - 1);   //Fuerza el índice de la BHT del GShare a tener la cantidad de bits que espacios de la BHT                                
            }
            else {
                reg = (reg << 1);               //Hubo un Not Taken, sólo aplica shift left al registro global
                hist = reg & gh_bits;           //Fuerza a la historia global con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT del GShare como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id_G = id_g & (counters - 1);   //Fuerza el índice de la BHT del GShare a tener la cantidad de bits que espacios de la BHT
            }
        }
        else if(states_G[id_G] == "ST"){        //Revisa si el estado del contador es Stringly Taken                          
            predic = "T";                       //Si cumple lo anterior predice un Taken
            prediction_G.push_back(predic);     //Guarda la predicción en su respectivo vector
            if(prediction_G[i] != jumps[i]){    //Revisa si la predicción es distinto al salto
                states_G[id_G] = "WT";          //Actualiza el estado Weakly Taken
            }
            if(jumps[i] == "T"){                //Revisa si el salto fue un Taken
                reg = (reg << 1) + 1;           //Aplica shift left al registro global y le suma 1
                hist = reg & gh_bits;           //Fuerza a la historia global con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT del GSharecomo la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id_G = id_g & (counters - 1);   //Fuerza el índice de la BHT del GShare a tener la cantidad de bits que espacios de la BHT                                
            }
            else {
                reg = (reg << 1);               //Hubo un Not Taken, sólo aplica shift left al registro global
                hist = reg & gh_bits;           //Fuerza a la historia global con una AND a ser de gh bits
                id_g = hist ^ address[i+1];     //Calcula el próximo índice de la BHT del GShare como la XOR del registro global con la siguiente máscara de s bits de la dirección de memoria
                id_G = id_g & (counters - 1);   //Fuerza el índice de la BHT del GShare a tener la cantidad de bits que espacios de la BHT
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //Tournament
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        id_meta = address[i];                                                   //El índice para los contadores del meta predictor es la máscara de s bits de la dirección de memoria
        if((states_meta[id_meta] == 2) || (states_meta[id_meta] == 3)){         //Revisa si el estado es Weakly Prefer PShare o Strongly Prefer PShare
            final_pred.push_back(prediction_G[i]);                              //Si lo anterior se cumple, se escoge al PShare
            if(prediction_G[i] == jumps[i]){                                    //Revisa si la predicción del PShare es igual al salto
                final_check.push_back("correct");                               //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
            }
            else{
                final_check.push_back("incorrect");                             //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
            }
        }
        else if((states_meta[id_meta] == 0) || (states_meta[id_meta] == 1)){    //Revisa si el estado es Weakly Prefer GShare o Strongly Prefer GShare
            final_pred.push_back(prediction_P[i]);                              //Si lo anterior se cumple, se escoge al GShare
            if(prediction_P[i] == jumps[i]){                                    //Revisa si la predicción del GShare es igual al salto
                final_check.push_back("correct");                               //Sí cumple la comparación; guarda que la predicción fue correcta en su respectivo vector
            }
            else{
                final_check.push_back("incorrect");                             //No cumple la comparación; guarda que la predicción fue incorrecta en su respectivo vector
            }
        }
        if(prediction_P[i] != prediction_G[i]){                                 //Revisa que la predicción del PShare sea distinta a la del GShare
            if(prediction_G[i] == jumps[i]){                                    //Revisa si la predicción del GShare es igual al salto
                if(states_meta[id_meta] < 3){                                   //Revisa si no está en el Strongly Prefer PShare
                    states_meta[id_meta]++;                                     //Avance al siguiente estado
                }
            }
            else {
                if(states_meta[id_meta] > 0){                                   //Revisa si no está en el Strongly Prefer GShare
                    states_meta[id_meta]--;                                     //Se devuelve al estado anterior
                }
            }
        }
    }
    if(this->out == 1){     //Si la salida de la simulación es 1 llama a la función writeFile
        writeFile(decimal,jumps,final_pred,final_check,"tournament.txt",counters);
    }
}

//Método para la salida de la simulación de un predictor
void Predictor::writeFile(vector<string> decimal,vector<string> jumps,vector<string> prediction, vector<string> check, string name,int BHT){
    int correct_T = 0;          //Contador de Taken que fueron correctos
    int incorrect_T = 0;        //Contador de Taken que fueron incorrectos
    int correct_N = 0;          //Contador de Not Taken que fueron correctos
    int incorrect_N = 0;        //Contador de Not Taken que fueron incorrectos
    ofstream file;              //Instanciación de un objeto de la clase fstream
    file.open(name,ios::out);   //Apertura del archivo para escribir
    //Escritura de las columnas en el archivo
    file << "PC" << "           " << "Outcome" << "     " << "Prediction" << "      " << "correct/incorrect" << endl;
    for(size_t i = 0; i < address.size(); i++){
        //Escritura de los resultados en el archivo
        file << decimal[i] << "     " << jumps[i] << "              " << prediction[i] << "             " << check[i] << endl;
        if((check[i] == "correct") && (prediction[i] == "T")){              //Revisa si la predicción fue Taken y está correcta
            correct_T++;                                                    //Suma 1 al contador correcto de Taken
        }
        else if((check[i] == "incorrect") && (prediction[i] == "T")){       //Revisa si la predicción fue Taken y está incorrecta
            incorrect_T++;                                                  //Suma 1 al contador incorrecto de Taken
        }
        else if((check[i] == "correct") && (prediction[i] == "N")){         //Revisa si la predicción fue Not Taken y está correcta
            correct_N++;                                                    //Suma 1 al contador correcto de Not Taken
        }
        else if((check[i] == "incorrect") && (prediction[i] == "N")){       //Revisa si la predicción fue Not Taken y está incorrecta
            incorrect_N++;                                                  //Suma 1 al contador incorrecto de Not Taken
        }
    } 
    file.close();               //Cierre del archivo
    float right_pred = float(correct_T+correct_N)/float(check.size())*100;  //Cálculo del porcentaje correcto de saltos
    //A partir de aquí sólo se imprimen resultados en consola
    cout << "----------------------------------------------------------" << endl;
    cout << "Prediction parameters" << endl;
    cout << "----------------------------------------------------------" << endl;
    switch (this->bp){
        case 0:
            cout << "Branch prediction type:                           Bimodal" << endl;
            break;
        case 1:
            cout << "Branch prediction type:                           PShare" << endl;
            break;
        case 2:
            cout << "Branch prediction type:                           GShare" << endl;
            break;
        case 3:
            cout << "Branch prediction type:                           Tournament" << endl;
            break;
        default:
            cout << "Branch prediction type:                           -" << endl;
    }
    cout << "BHT size (entries):                               " << BHT << endl;
    cout << "Global history register size:                     " << this->gh << endl;
    cout << "Private history register size:                    " << this->ph << endl;
    cout << "----------------------------------------------------------" << endl;
    cout << "Simulation results:" << endl;
    cout << "----------------------------------------------------------" << endl;
    cout << "Number of branches:                               " << check.size() <<endl;
    cout << "Number of correct prediction of taken branches:   " << correct_T << endl;
    cout << "Number of incorrect prediction of taken branches: " << incorrect_T << endl;
    cout << "Correct prediction of not taken branches:         " << correct_N <<endl;
    cout << "Incorrect prediction of not taken branches:       " << incorrect_N << endl;
    cout << "Percentage of correct predictions                 " << right_pred << " %" << endl;
    cout << "----------------------------------------------------------" << endl;
}