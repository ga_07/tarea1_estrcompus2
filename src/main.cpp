#include "Predictor.h"
#include <ctime>

using namespace std;
unsigned t0, t1;

int main(int argc, char *argv[]){
    t0 = clock();
    int s = stoi(argv[2]);
    int bp = stoi(argv[4]);
    int gh = stoi(argv[6]);
    int ph = stoi(argv[8]);
    int out = stoi(argv[10]);
    Predictor prediction(s,bp,gh,ph,out);
    prediction.read();
    switch (bp){
        case 0:
            prediction.bimodal();
            break;
        case 1:
            prediction.pshare();
            break;
        case 2:
            prediction.gshare();
            break;
        case 3:
            prediction.tournament();
            break;
        default:
            cout << "No seleccionó ninguno de los predictores disponibles" << endl;
            break;
    }
    t1 = clock();
    double execTime = (double(t1-t0)/CLOCKS_PER_SEC);
    cout << "Excecution time: " << execTime << " s"<< endl;
    return 0;
}